﻿using SignalRChatServer.Models;

namespace SignalRChatServer.Data
{
    public static class ClientSource
    {
        public static List<Client> Clients { get; set; } = new List<Client>();
    }
}
